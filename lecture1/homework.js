// By Mantas Petrikas

function test() {
    //1. Write two binary function`s, add and mul, that take two numbers and return their sum and product. 
    assert(add(3, 4), 7);     
    assert(mul(3, 4), 12);  

    //2. Write a function that takes an argument and returns a function that returns that argument.  
    var idf = identityf (3);  
    assert(idf(), 3);
    
    //3. Write a function that adds from two invocations.
    assert(addf(3)(4), 7);
    
    //4. Write a function that takes a binary function, and makes it callable with two invocations.  
    var addf2 = applyf (add); // had to rename to addf2 since there is function named addf
    assert(addf2(3)(4), 7); 
    assert(applyf (mul)(5)(6), 30); 
    
    //5. Write a function that takes a function and an argument, and returns a function that can supply a second argument.  
    var add3 = curry(add, 3);  
    assert(add3(4), 7); 
    assert(curry(mul , 5)(6), 30); 
    
    //6. Without writing any new functions, show three ways to create the inc function.
    let inc = addf(1);
    assert(inc(5), 6); 
    inc = applyf(add)(1);
    assert(inc(10), 11);
    inc = curry(add, 1);
    assert(inc(13), 14);
    
    //7. Write a function twice that takes a binary function and returns a unary function that passes its argument to the binary function twice. 
    var double = twice(add);
    assert(double(11), 22); 
    var square = twice(mul);  
    assert(square(11), 121);
} 

function add(x, y){
    return x + y;  
}
 
function mul(x, y){
    return x * y;
}

function identityf(value){
    return () => value;
}

function addf(x){
    return (y) => x + y;
}

function applyf(func){
    return (arg1) => { 
        return (arg2) => func(arg1, arg2);
    };
}

function curry(func, arg1){
    return (arg2) => { 
        return func(arg1, arg2);
    };
}

function twice(func){
    return (arg) => func(arg, arg);
}

function assert(value, expectedValue){
    console.log((value === expectedValue ? "✔ " : "✘ ") + "expected value " + expectedValue.toString().padStart(3) + ", actual value " + value.toString().padStart(3));
}   