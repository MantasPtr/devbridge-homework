var defaultList = ['Style the HTML','Take out the trash','Vacuum the carpet','Make dinner'];
createDefaultList(defaultList);

function createDefaultList(itemList){
    let list = document.querySelector('.js-add-task-list');
    itemList.forEach(
        (text) => list.appendChild(createTodoItem(text))
    );
}

function deleteTodoItem(event){
    event.stopPropagation();
    event.target.parentElement.remove();
}

document.querySelector('.js-add-task-form').addEventListener('submit', addTodoItem);

function addTodoItem(event){
    //prevent default submit behavior, which causes page to reload
    event.preventDefault();

    //get entered text. If it is empty, do nothing.
    let inputField = document.querySelector('.js-add-task-input');
    let newTodoText = inputField.value;
    //using Type coercion below
    if(!newTodoText){
        return;
    }

    let newTodoItem = createTodoItem(newTodoText);

    //append new todo item into todo list
    document.querySelector('.js-add-task-list').appendChild(newTodoItem);

    // clear input field
    inputField.value = null;
}

function createTodoItem(text) {
    //create new list item (li) element
    let newTodoItem = createLi();
    newTodoItem.appendChild(createTextSpan(text));
    newTodoItem.appendChild(createDeleteButton());
    newTodoItem.appendChild(createDoneButton());
    return newTodoItem;
}

function createLi(){
    let newTodoItem = document.createElement('li');
    newTodoItem.classList.add('list-group-item');
    return newTodoItem;
}

function createTextSpan(text){
    let todoText = document.createElement('span');
    todoText.classList.add('badge', 'badge-info');
    todoText.textContent = text;
    return todoText;
}

function createDeleteButton(){
    let deleteButton = document.createElement('button');
    deleteButton.classList.add('btn', 'btn-danger', 'oi', 'oi-delete', 'right', 'js-delete');
    deleteButton.title = 'Delete';
    deleteButton.setAttribute('aria-hidden', 'true');
    //add event listener so it would be functional
    deleteButton.addEventListener('click', deleteTodoItem);
    return deleteButton;
}

function createDoneButton(){
    // <button class="btn btn-success oi oi-check right js-delete mr10" title="Done" aria-hidden="true"/>
    let doneButton = document.createElement('button');
    doneButton.classList.add('btn', 'btn-success', 'oi', 'oi-check', 'mr10' ,'right', 'js-done');
    doneButton.title = 'Done';
    doneButton.setAttribute('aria-hidden', 'true');
    doneButton.addEventListener('click', markTaskAsDone);
    return doneButton;
}

function markTaskAsDone(event){
    event.stopPropagation();
    let todoItem = event.target.parentElement;
    todoItem.classList.add('done');
    todoItem.querySelector('.js-delete').classList.add('hidden');
    todoItem.querySelector('.js-done').classList.add('hidden');
    todoItem.appendChild(createUndoButton());
}

function undoTask(event) {
    event.stopPropagation();
    let todoItem = event.target.parentElement;
    todoItem.classList.remove('done');
    todoItem.querySelector('.js-delete').classList.remove('hidden');
    todoItem.querySelector('.js-done').classList.remove('hidden');
    event.target.remove();
}

function createUndoButton(){
    //<button class="btn btn-secondary oi oi-action-redo right js-delete mr10" title="Undo" aria-hidden="true"/>
    let undoButton = document.createElement('button');
    undoButton.classList.add('btn', 'btn-secondary', 'oi', 'oi-action-undo','right', 'js-undo');
    undoButton.title = 'Undo';
    undoButton.setAttribute('aria-hidden', 'true');
    undoButton.addEventListener('click', undoTask);
    return undoButton;
}
