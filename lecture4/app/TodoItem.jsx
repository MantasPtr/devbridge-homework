import React from 'react';

class TodoItem extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
      return <li>
        {this.props.title} - 
        <span onClick={() => this.props.deleteItem(this.props.id)}>Delete</span>
        <input checked={this.props.checked} type="checkbox" onChange={this.props.onStatusChange} />
      </li>
    }
}

export default TodoItem;