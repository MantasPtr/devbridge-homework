import React from 'react';
import { render } from 'react-dom';
import TodoItem from './TodoItem';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from './actions';

class TodoItemList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputValue: '',
    };
  }

  addTodoItem (e){
    e.preventDefault();
    //id cannot start with a number, su add a letter at the begining 
    const newId = 'tdi-' + Math.random().toString(36).substring(2, 15);
    const title = this.state.inputValue;
    this.props.dispatchAddTodoItem(newId, title);
  }

  deleteItem(itemId){
    this.props.dispatchDeleteTodoItem(itemId);
  }

  changeItemStatus(itemId){
    this.props.dispatchChangeTodoItemStatus(itemId);
  }

  inputChanged(e){
    this.setState({
      inputValue: e.target.value
    });
  }
  

  render() {
    const items = this.props.itemList.map(item => {
      return <TodoItem 
                key={item.id}
                id={item.id}
                title={item.title}
                checked={item.checked}
                deleteItem={() => this.deleteItem(item.id)}
                onStatusChange={() => this.changeItemStatus(item.id)}
              />
    });

    return <div>
            <h1>{this.props.header}</h1>
            <form onSubmit={this.addTodoItem.bind(this)}>
              <input type="text" onChange={this.inputChanged.bind(this)} value={this.state.inputValue}/>
            </form>

            <div>
              <ul>
                {items}
              </ul>
            </div>
        </div>
  }
}

export default connect(
  (state) => ({
      itemList: state.todoItems
  }),
  (dispatch) => bindActionCreators({
      dispatchAddTodoItem: actions.addItem,
      dispatchDeleteTodoItem: actions.deleteItem,
      dispatchChangeTodoItemStatus: actions.tickItem,
  }
  ,dispatch)
)(TodoItemList)