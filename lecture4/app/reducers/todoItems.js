export default (state = [], action) => {
    switch (action.type) {
        case "ADD_ITEM": 
            return  [
                ...state,
                {
                    id:action.itemId,
                    title:action.title,
                    checked: false,
                }];
        case "DELETE_ITEM": 
            return  [...state.filter(item => item.id !== action.itemId)];
        case "TICK_ITEM":
            return [...state.map(item => item.id === action.itemId ? {id: item.id, title: item.title , checked: !item.checked} :  item)];       
        default:
            return state;
    }
};
