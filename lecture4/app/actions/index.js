
export const changeColor = (colorHexCode) => {
    return {
        type: 'CHANGE_COLOR',
        colorHexCode
    }
};

export const addItem = (itemId, title) => {
    return {
        type: 'ADD_ITEM',
        itemId,
        title,
    }
};

export const deleteItem = (itemId) => {
    return {
        type: 'DELETE_ITEM',
        itemId,
    }
};

export const tickItem = (itemId) => {
    return {
        type: 'TICK_ITEM',
        itemId,
    }
};