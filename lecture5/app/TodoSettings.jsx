import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from './actions';

class TodoSettings extends React.Component {
    constructor(props) {
        super(props);

        this.onChangeColor = this.onChangeColor.bind(this);
    }

    render() {
        return (
            <div className="app-color-picker">
                <div>
                    <span>App Color</span>
                </div>
                <input
                    type="color"
                    value={ this.props.appSettings.backgroundColor}
                    onChange={this.onChangeColor}
                />
            </div>
        );
    }

    onChangeColor(e) {
        this.props.dispatchChangeColor(e.target.value);
    }
}

export default connect(
    (state) => ({
        appSettings: state.settings
    }),
    (dispatch) => bindActionCreators({
        dispatchChangeColor: actions.setColor
    }
    ,dispatch)
)(TodoSettings)