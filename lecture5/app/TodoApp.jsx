import React from 'react';
import TodoAppNavigation from './TodoAppNavigation';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from './actions';

class TodoApp extends React.Component {
  constructor(props) {
     super(props);
     this.props.dispatchGetSettings();
 }
  render() {
    const appStyle = {
        backgroundColor: this.props.appSettings.backgroundColor
    };


    return <div>
              <div className="todo-app" style={appStyle}>
                <TodoAppNavigation />
                {this.props.children}
                {this.props.isLoading ? <span>LOADING...</span> : null}
              </div>
          </div>
  }
}

export default connect(
  (state) => ({
    appSettings: state.settings,
    isLoading: state.todoItems.loading
  }),
  (dispatch) => bindActionCreators({
    dispatchGetSettings: actions.loadColor
  },
  dispatch)
)(TodoApp)