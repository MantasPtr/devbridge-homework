import React from 'react';
import EditableField from './EditableField'
class TodoItem extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return <li>
                <EditableField title={this.props.title} onSaveChanges={(value) => this.props.onSaveChanges(this.props.id, value)}/>
                &nbsp;-&nbsp; 
                <span onClick={() => this.props.deleteItem(this.props.id)}>Delete</span>
                <input checked={this.props.checked} type="checkbox" onChange={() => this.props.onStatusChange(this.props.id)} />      
            </li>
    }
}

export default TodoItem;