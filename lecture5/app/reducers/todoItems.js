export default (state = { items: [], loading: false }, action) => {
    switch (action.type) {
        case 'SET_LOADING':
            return {
                ...state,
                loading: true
            };

        case 'REMOVE_LOADING':
            return {
                ...state,
                loading: false
            };
        case 'LOAD_ALL':
            return {
                ...state,
                loading: false,
                items: action.items
            };
        case 'ADD_NEW':
            return {
                ...state,
                loading: false,
                items: [
                    ...state.items,
                    action.todo
                ]
            };
        case 'DELETE_ITEM':
            return {
                ...state,
                loading: false,
                items: state.items.filter(item => item.id !== action.itemId)
            } 
        case 'TOGGLE_ITEM':
            return {
                ...state,
                loading: false,
                items: state.items.map((item) => item.id === action.itemId ? {...item, completed: !item.completed} : item ),
            }
        case 'EDIT_ITEM':
            return {
                ...state,
                loading: false,
                items: state.items.map((item) => item.id === action.oldId ? {...item, id: action.newId, title: action.newTitle} : item ),
            }
        default:
            return state;
    }
};
