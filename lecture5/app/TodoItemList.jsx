import React from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import TodoItem from './TodoItem';
import * as actions from './actions';

class TodoItemList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputValue: ''
    };
    this.props.dispatchLoadAllItems();
  }

  addTodoItem (e){
    e.preventDefault();
    const title = this.state.inputValue;
    this.setState({ inputValue: '' });
    this.props.dispatchAddNewItem(title);
  }

  deleteItem(itemId){
    this.props.dispatchDeleteItem(itemId);
  }

  toggleItem(itemId){
    this.props.dispatchToggleItem(itemId);
  }

  inputChanged(e){
    this.setState({
      inputValue: e.target.value
    });
  }
  
  changeItemValue(id, value){
    this.props.dispatchEditItem(id, value)
  }

  render() {
    const {todoItems, isLoading} = this.props;

    const items = todoItems.map(item => {
      return <TodoItem 
                key={item.id}
                id={item.id}
                title={item.title}
                deleteItem={this.deleteItem.bind(this)}
                onSaveChanges={this.changeItemValue.bind(this)}
                checked={item.completed}
                onStatusChange={this.toggleItem.bind(this)}
              />
    });

    return <div>
            <h1>{this.props.header}</h1>
            <form onSubmit={this.addTodoItem.bind(this)}>
              <input type="text" onChange={this.inputChanged.bind(this)} value={this.state.inputValue}/>
            </form>
            <div>
              <ul>
                {items}
              </ul>
            </div>
        </div>
  }
}

export default connect(
  (state) => ({
      todoItems: state.todoItems.items,
  }),
  (dispatch) => bindActionCreators({
      dispatchAddNewItem: actions.addNewItem,
      dispatchLoadAllItems: actions.loadAll,
      dispatchDeleteItem: actions.deleteItem,
      dispatchEditItem: actions.editItem,
      dispatchToggleItem: actions.toggleItem,
  }
  ,dispatch)
)(TodoItemList)