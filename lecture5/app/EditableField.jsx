import React from 'react';


class EditableField extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            editable: false,
            value: this.props.title
        }
    }

    makeEditable(){
        this.setState({...this.state, editable: true});
    }
    
    finishEditing(){
        this.setState({...this.state, editable:  false});
        this.props.onSaveChanges(this.state.value);
    }

    editValue(e){
        this.setState({...this.state, value: e.target.value});
    }


    handleKeyPress(e) {
        if (e.key === 'Enter') {
            this.finishEditing()
        }
    }

    render() {
        if (this.state.editable) { 
           return <input autoFocus value={this.state.value} onBlur={this.finishEditing.bind(this)} onChange={this.editValue.bind(this)} onKeyPress={this.handleKeyPress.bind(this)} />
        } else { 
           return <span onClick={this.makeEditable.bind(this)}>{this.props.title}</span>
        }
    }
}

export default EditableField;