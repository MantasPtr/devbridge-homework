import api from '../server/remoteApi';

export const changeColor = (colorHexCode) => {
    return {
        type: 'CHANGE_COLOR',
        colorHexCode
    }
};

export const setColor = (colorHexCode) => (dispatch) => {
    dispatch(setLoading());
    api.getAppSettings()
        .then((setings) => {
            api.updateAppSettings({...setings, appColor:  colorHexCode})
                .then(() => {
                    dispatch(changeColor(colorHexCode));
                    dispatch ({
                        type: 'REMOVE_LOADING',
                    });
                });
        });

}

export const loadColor = () => (dispatch) => {
    dispatch(setLoading());
    api.getAppSettings()
        .then((settings) => {
            dispatch ({
                type: 'LOAD_COLOR',
                color: settings.appColor,
            });
            dispatch ({
                type: 'REMOVE_LOADING',
            });
        });
};

export const addNewAction = (todoItem) => {
    return {
        type: 'ADD_NEW',
        todo: todoItem
    }
}

export const setLoading = () => {
    return {
        type: 'SET_LOADING'
    }
}

export const addNewItem = title => (dispatch) => {
    dispatch(setLoading());
    api.addAppTodo(title)
        .then((newTodoId) => {
            api.getAppTodoById(newTodoId)
                .then((response) => {
                    dispatch(addNewAction(response.todo));
                });
        });
};

export const editItem = (id, value) => (dispatch) => {
    dispatch(setLoading());
    api.addAppTodo(value)
        .then((newId) => {
            api.removeAppTodo(id)
                .then(() => {
                    dispatch({
                         type: 'EDIT_ITEM',
                         oldId: id,
                         newId: newId,
                         newTitle: value,
                    });
                });
        });
};

export const toggleItem = (id) => (dispatch) => {
    dispatch(setLoading());
    api.toggleAppTodo(id)
        .then(() => {
            dispatch({
                type: 'TOGGLE_ITEM',
                itemId: id,
            });
        });
};

export const deleteItem = id => (dispatch) => {
    dispatch(setLoading());
    api.removeAppTodo(id)
        .then((response)=> {
            dispatch({
                type: 'DELETE_ITEM',
                itemId: id,
            });
        });
};


export const loadAll = () => (dispatch) => {
    dispatch(setLoading());
    api.getTasks()
        .then((response) => {
            dispatch({
                type: 'LOAD_ALL',
                items: response
            });
        });
};
