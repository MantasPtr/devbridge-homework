const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

const appDir = path.resolve(__dirname, 'app/');
const buildDir = path.resolve(__dirname, 'wwwroot/dist/');

const HTMLWebpackPluginConfig = new HtmlWebpackPlugin(
    {
        template: path.resolve(appDir, 'index.html'),
        hash: true,
        filename: 'index.html',
        inject: 'body'
    }
);


module.exports = {
    entry: {
        main: path.resolve(appDir,'index.jsx'),
    //app: path.resolve(appDir, 'index.jsx'),
    vendor: [
      'react',
      'react-dom'
    ]
  },
  output: {
    //filename: 'index_bundle.js',
      path: buildDir,
    publicPath       : '/dist/'
  },
  //mode: 'development',
  performance: {
    hints: false
  },
  plugins: [HTMLWebpackPluginConfig],
  devtool: 'cheap-module-source-map',
  devServer: {
    historyApiFallback: true,
    contentBase: buildDir,
    hot: true
  },
  resolve: {
    extensions: ['.js', '.jsx']
  },
    module: {
        loaders: [
            {
                include: appDir,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['env', 'react'],
                        plugins: [
                            'react-hot-loader/babel',
                            'babel-plugin-transform-class-properties',
                            'babel-plugin-transform-object-rest-spread'
                        ]
                    }                                                 
                }
            }
        ],
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          presets: ['react', 'env'],
          plugins: ['transform-object-rest-spread']
        }
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      }
      ]   
      
  }
};
