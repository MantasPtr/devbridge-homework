﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Todo.API.Controllers
{
    [Route("api/todos")]
    public class TodosController  : Controller
    {
        private static readonly List<Todo> _todos = new List<Todo>
            {
              new Todo {id = "id1", title = "learn js"},
              new Todo { id = "idLinas", title = "never touch js", completed = true}

            };

        [HttpGet]
        public IActionResult Get([FromQuery] string name, [FromQuery] bool? completed, [FromQuery] string sortDirection)
        {
            
            var query = from item in _todos select item;
            if (name != null)
                query = query.Where(t => t.title == name);
            if (completed != null)
                query = query.Where(t => t.completed == completed);
            if (sortDirection != null)
            {
                if (Enum.TryParse(sortDirection, out SortDirection sort))
                {
                    query = resolveSortDirection(sort, query);
                }
                else
                {
                    return BadRequest($"invalid sort direction. Posible values: {SortDirection.AtoZ} and {SortDirection.ZtoA}");
                }
            }

            return Ok(query.ToList());
        }

        private IEnumerable<Todo> resolveSortDirection(SortDirection sort, IEnumerable<Todo> query)
        {
                switch (sort)
                {
                    case SortDirection.AtoZ:
                        return query.OrderBy(item => item.title);
                    case SortDirection.ZtoA:
                        return query.OrderByDescending(item => item.title);
                    default:
                        throw new InvalidOperationException("SortDirection not implemented");
                }
        }

        [HttpGet("{id}")]
        public IActionResult GetById(string id)
        {
            if (String.IsNullOrWhiteSpace(id)) { return NoContent(); }

            var result = _todos.FirstOrDefault(todo => todo.id == id);
            if(result != null)
            {
                return Ok(result);
            }
            return NoContent();
        }

        [HttpPost, HttpPut]
        public IActionResult Post([FromBody]Todo todo)
        {
            // Validate
            if(todo == null) { return BadRequest("Can't insert null todo"); }
            if (String.IsNullOrWhiteSpace(todo.title)){ return BadRequest("Can't insert todo with empty title"); }


            // Init id if necessary
            if (todo.id == null) { todo.id = Guid.NewGuid().ToString(); }


            // Check if todo with such id already exists
            var existingTodo = _todos.FirstOrDefault(elem => elem.id == todo.id);


            // If there is not such todo - insert
            if(existingTodo == null)
            {
                _todos.Add(todo);
            }
            else 
            {
                _todos.Remove(existingTodo);
                _todos.Add(todo);
            }
            
            return Ok(todo.id);
        }

       


        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            var todoToDElete = _todos.FirstOrDefault(todo => todo.id == id);
            if(todoToDElete == null) { return NotFound($"Can't remove todo with id {id}"); }

            _todos.Remove(todoToDElete);
            return Ok();
        }
    }

    public enum SortDirection
    {
        AtoZ,
        ZtoA
    }

    public class Todo
    {
        public string id { get; set; }
        public string title { get; set; }
        public bool completed { get; set; }
    }
}

