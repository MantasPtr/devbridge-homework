# lecture5vno2018s

https://www.mocky.io/v2/5abb98dd2d000053009bddc0


Load Todo items from localStorage:
1. Extend todoItems reducer to add new items to its state (ADD_NEW only adds one item)
2. Create new action, that downloads (via remoteApi.getTasks()) and dispatches items
3. Call this new action from component only once


Display LOADING while communicating with server 
(remoteApi.js - 0.5 sec delay - you can increase it for testing):
1. Extend todoItems reducer to have flag (true/false) loading
2. Utilize Redux Thunk dispatch feature to dispatch few actions
3. Display Loading label if flag in reducer is set to true
