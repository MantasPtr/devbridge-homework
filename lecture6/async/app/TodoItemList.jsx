import React from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import TodoItem from './TodoItem';
import * as actions from './actions';

class TodoItemList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputValue: ''
    };
    this.props.dispatchLoadAllItems();
  }

  addTodoItem (e){
    e.preventDefault();
    const title = this.state.inputValue;
    this.setState({ inputValue: '' });
    this.props.dispatchAddNewItem(title);
  }

  deleteItem(itemId){
    let newList = this.state.todoItemList.filter(item => item.id !== itemId);
    this.setState({todoItemList: newList});
  }

  inputChanged(e){
    this.setState({
      inputValue: e.target.value
    });
  }

  render() {
    const {todoItems, isLoading} = this.props;

    const items = todoItems.map(item => {
      return <TodoItem 
                key={item.id}
                id={item.id}
                title={item.title}
                deleteItem={this.deleteItem.bind(this)}
              />
    });

    return <div>
            <h1>{this.props.header}</h1>
            <form onSubmit={this.addTodoItem.bind(this)}>
              <input type="text" onChange={this.inputChanged.bind(this)} value={this.state.inputValue}/>
            </form>
            {isLoading ? <div>Loading...</div> : null}
            <div>
              <ul>
                {items}
              </ul>
            </div>
        </div>
  }
}

export default connect(
  (state) => ({
      todoItems: state.todoItems.items,
      isLoading: state.todoItems.loading
  }),
  (dispatch) => bindActionCreators({
      dispatchAddNewItem: actions.addNewItem,
      dispatchLoadAllItems: actions.loadAll
  }
  ,dispatch)
)(TodoItemList)