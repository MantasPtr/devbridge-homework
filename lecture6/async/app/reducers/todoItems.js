export default (state = { items: [], loading: false }, action) => {
    switch (action.type) {
        case 'SET_LOADING':
            return {
                ...state,
                loading: true
            };
        case 'LOAD_ALL':
            return {
                ...state,
                loading: false,
                items: action.items
            };
        case 'ADD_NEW':
            return {
                ...state,
                loading: false,
                items: [
                    ...state.items,
                    action.todo
                ]
            };
        default:
            return state;
    }
};
