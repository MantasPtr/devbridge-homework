import api from '../server/remoteApi';

export const changeColor = (colorHexCode) => {
    return {
        type: 'CHANGE_COLOR',
        colorHexCode
    }
};

export const addNewAction = (todoItem) => {
    return {
        type: 'ADD_NEW',
        todo: todoItem
    }
}

export const setLoading = () => {
    return {
        type: 'SET_LOADING'
    }
}

export const addNewItem = title => (dispatch) => {
    dispatch(setLoading());
    api.addAppTodo(title)
        .then((newTodoId) => {
            api.getAppTodoById(newTodoId)
                .then((response) => {
                    dispatch(addNewAction(response.todo));
                });
        });
};

export const loadAll = () => (dispatch) => {
    dispatch(setLoading());
    api.getTasks()
        .then((response) => {
            dispatch({
                type: 'LOAD_ALL',
                items: response
            });
        });
};
