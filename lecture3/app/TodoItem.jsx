import React from 'react';
import {Checkbox, IconButton, Tooltip}  from 'material-ui';
import {ListItem, ListItemText} from 'material-ui/List';
import DeleteIcon from 'material-ui-icons/Delete';

export default class TodoItem extends React.Component {
  
    constructor(props) {
        super(props);
        this.state = {
            done: false,
        };
    };

    changeState() {
        this.setState({done: !this.state.done});
    }
    
    render() {
     return <ListItem>
         
                <Checkbox checked={this.state.done} onChange={this.changeState.bind(this)}/>
                <ListItemText primary={this.props.title}/>
                <Tooltip title={this.state.done ? "What's done is done" :"Delete"}>
                    <div> 
                        <IconButton disabled={this.state.done} onClick={() => this.props.deleteItem(this.props.id)}>
                            <DeleteIcon aria-label="Delete"/>
                        </IconButton>
                    </div>
                </Tooltip>
            </ListItem>
    }
}