import React from 'react';
import { render } from 'react-dom';
import TodoApp from './TodoApp.jsx';
import './styles.css';
import { createMuiTheme } from 'material-ui/styles';
import blue from 'material-ui/colors/blue';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

const theme = createMuiTheme({
  palette: {
    primary: blue,
  },
});


render(
<MuiThemeProvider theme={theme  }>
    <TodoApp header="Todo app"/>
</MuiThemeProvider>
, document.querySelector('.react-app'));