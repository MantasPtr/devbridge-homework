import React from 'react';
import { render } from 'react-dom';
import TodoItem from './TodoItem';
import {AppBar, List, Input} from 'material-ui'
export default class TodoApp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputValue: '',
      todoItemList: [], 
    };
  }

  componentDidMount(){
    //example items
    this.addTodoItems(["nothing","devbridge homework"]);
  }



  onSubmit(e){
    e.preventDefault();
    this.addTodoItems([this.state.inputValue]);
    this.clearInput();
  }

  clearInput(){
    this.setState({inputValue:''})
  }

  addTodoItems (itemTexts){ 
    //id cannot start with a number, su add a letter at the begining 
    let newItems = itemTexts.map(text => this.createTodoItem(text));
    this.setState({
      todoItemList: [
        ...this.state.todoItemList, 
        ...newItems,
      ]
    });
  }

  createTodoItem(itemText){
    let newId = 'tdi-' + Math.random().toString(36).substring(2, 15);
    return <TodoItem 
            key={newId}
            id={newId}
            title={itemText}
            deleteItem={this.deleteItem.bind(this)}
          />; 
  }

  deleteItem(itemId){
    let newList = this.state.todoItemList.filter(item => item.props.id !== itemId);
    this.setState({todoItemList: newList});
  }

  inputChanged(e){
    this.setState({
      inputValue: e.target.value
    });
  }

  render() {
    return <div className="centered">
            <h1>{this.props.header}</h1>
            <form onSubmit={this.onSubmit.bind(this)}>
                <Input className="full-width" placeholder='What are you planing to do next?' onChange={this.inputChanged.bind(this)} value={this.state.inputValue}/>
            </form>
            <div>
              <List >
                {this.state.todoItemList}
              </List>
            </div>
        </div>
  }
}
